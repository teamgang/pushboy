import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'package:poboy/l10n/localization.dart';
import 'package:poboy/utils/boi_commands.dart';
import 'package:poboy/utils/boi_listeners.dart';
import 'package:poboy/utils/metric_reporter.dart';
import 'package:poboy/widgets/router/router.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

class MyApp extends StatelessWidget {
  const MyApp({required this.king});
  final King king;

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => this.king,
      lazy: false,
      child: LocalizationsWrapper(),
    );
  }
}

class LocalizationsWrapper extends StatefulWidget {
  @override
  LocalizationsWrapperState createState() => LocalizationsWrapperState();
}

class LocalizationsWrapperState extends State<LocalizationsWrapper> {
  @override
  initState() {
    super.initState();
    BoiCommands.syncAllSettings(context);
    createListeners(context);
    MetricReporter.tryToReportAllFailedBoiMetrics(context: context);
  }

  @override
  Widget build(BuildContext context) {
    final king = King.of(context);
    final todd = king.todd;

    return Observer(
      builder: (_) => MaterialApp(
        navigatorKey: king.navigatorKey,
        onGenerateRoute: (settings) => route(context, settings),
        initialRoute: Routes.home,
        localizationsDelegates: const [
          AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        locale: Locale(todd.locale, ''),
        supportedLocales: const [
          Locale('en', ''),
          //Locale('ru', ''),
        ],
        onGenerateTitle: (BuildContext context) =>
            AppLocalizations.of(context)?.title ?? 'PushBoi',
        debugShowCheckedModeBanner: false,
        theme: king.theme.themeData,
      ),
    );
  }
}
