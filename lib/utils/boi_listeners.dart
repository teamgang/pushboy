import 'dart:io';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

import 'package:poboy/utils/boi_commands.dart';
import 'package:poboy/utils/metric_reporter.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

void createListeners(BuildContext context) {
  //fires when notification created
  AwesomeNotifications()
      .createdStream
      .listen((ReceivedNotification notification) {
    final bp = BoiPayload.fromPayload(notification.payload ?? {});

    //NOTE: we must come to this point to capture the BuildContext
    switch (bp.plasticity) {
      case BoiPlasticity.commandSyncAllSettings:
        BoiCommands.syncAllSettings(context);
        break;

      case BoiPlasticity.custom:
      case BoiPlasticity.static_:
      case BoiPlasticity.limitless:
        bp.metricCreated(context);
        MetricReporter.tryToReportAllFailedBoiMetrics(context: context);
        break;

      case BoiPlasticity.none:
        throw Exception('BoiPlasticity is none! boiId; ${bp.boiId}');
    }
  });

  // fires when notification displayed on system status bar
  AwesomeNotifications()
      .displayedStream
      .listen((ReceivedNotification notification) {
    final bp = BoiPayload.fromPayload(notification.payload ?? {});
    bp.metricDisplayed(context);
    MetricReporter.tryToReportAllFailedBoiMetrics(context: context);
  });

  // fires when notification tapped by user
  AwesomeNotifications()
      .actionStream
      .listen((ReceivedNotification notification) {
    if (notification.channelKey == 'static' && Platform.isIOS) {
      AwesomeNotifications().getGlobalBadgeCounter().then(
            (value) => AwesomeNotifications().setGlobalBadgeCounter(value - 1),
          );
    }

    final bp = BoiPayload.fromPayload(notification.payload ?? {});
    bp.metricTapped(context);
    MetricReporter.tryToReportAllFailedBoiMetrics(context: context);
  });

  // fires when notification dismissed by user
  AwesomeNotifications()
      .dismissedStream
      .listen((ReceivedNotification notification) {
    final bp = BoiPayload.fromPayload(notification.payload ?? {});
    bp.metricDismissed(context);
    MetricReporter.tryToReportAllFailedBoiMetrics(context: context);
  });
}

class BoiPayload {
  final String boiId;
  final String channelId;
  final String networkId;
  final BoiPlasticity plasticity;
  const BoiPayload({
    required this.boiId,
    required this.channelId,
    required this.networkId,
    required this.plasticity,
  });

  factory BoiPayload.fromPayload(Map<String, String> pl) {
    return BoiPayload(
      boiId: pl['boi_id'] ?? '',
      channelId: pl['channel_id'] ?? '',
      networkId: pl['network_id'] ?? '',
      plasticity: boiPlasticityFromString(pl['plasticity'] ?? ''),
    );
  }

  metricCreated(BuildContext context) async {
    MetricReporter.tryToReportBoiMetric(
      boiId: boiId,
      context: context,
      metricType: MetricType.boiCreated,
    );
  }

  metricDisplayed(BuildContext context) async {
    MetricReporter.tryToReportBoiMetric(
      boiId: boiId,
      context: context,
      metricType: MetricType.boiDisplayed,
    );
  }

  metricTapped(BuildContext context) async {
    MetricReporter.tryToReportBoiMetric(
      boiId: boiId,
      context: context,
      metricType: MetricType.boiTapped,
    );
  }

  metricDismissed(BuildContext context) async {
    MetricReporter.tryToReportBoiMetric(
      boiId: boiId,
      context: context,
      metricType: MetricType.boiDismissed,
    );
  }
}
