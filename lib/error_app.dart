import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ErrorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(children: const <Widget>[
            SizedBox(height: 40),
            Text20('PushBoi'),
            SizedBox(height: 4),
            Text20(
                'There was an error during startup. Try restarting PushBoi. You might need to be connected to the internet. If you need help, please contact us at team@pushboi.io',
                maxLines: 8),
          ]),
        ),
      ),
    );
  }
}
