import 'package:flutter/material.dart';

import 'package:poboy/widgets/boi/show.dart';
import 'package:poboy/widgets/help_page.dart';
import 'package:poboy/widgets/home_page.dart';
import 'package:poboy/widgets/warnings_page.dart';

import 'package:poboy/widgets/channel_sub/overview_page.dart';
import 'package:poboy/widgets/channel_sub/settings_page.dart';

import 'package:poboy/widgets/network_sub/add_sub_page.dart';
import 'package:poboy/widgets/network_sub/overview_page.dart';
import 'package:poboy/widgets/network_sub/settings_page.dart';

import 'package:poboy/widgets/sign_in/sign_in_page.dart';
import 'package:poboy/widgets/sign_in/confirm_email_page.dart';

import 'package:poboy/widgets/surfer/account_page.dart';
import 'package:poboy/widgets/surfer/change_email_request.dart';
import 'package:poboy/widgets/surfer/change_password_request.dart';
import 'package:poboy/widgets/surfer/confirm_delete_account_page.dart';
import 'package:poboy/widgets/surfer/logged_out_change_password_request.dart';
import 'package:poboy/widgets/surfer/surfer_page_old.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

Route<dynamic> route(BuildContext context, RouteSettings settings) {
  String name = settings.name ?? '';
  King.of(context).routerContext = context;

  return MaterialPageRoute(
      settings: settings,
      builder: (c) {
        var args = getProcessedArgs(settings);

        switch (name) {
          case Routes.confirmEmail:
            return ConfirmEmailPage();

          case Routes.boiShow:
            return BoiShowPage(boi: args.boi);

          case Routes.networkSubAdd:
            return NetworkSubAddPage();

          case Routes.channelSubSettings:
            return ChannelSubSettingsPage(channelSub: args.channelSub);
          case Routes.channelSubOverview:
            return ChannelSubOverviewPage(channelSub: args.channelSub);

          case Routes.networkSubSettings:
            return NetworkSubSettingsPage(networkSub: args.networkSub);
          case Routes.networkSubOverview:
            return NetworkSubOverviewPage(networkSub: args.networkSub);

          case Routes.account:
            return const AccountPage();
          case Routes.surfer:
            return const SurferPage();
          case Routes.surferConfirmDeleteAccount:
            return SurferConfirmDeleteAccountPage(surfer: args.surfer);
          case Routes.surferChangeEmailRequest:
            return SurferChangeEmailRequestPage(surfer: args.surfer);
          case Routes.surferChangePasswordRequest:
            return SurferChangePasswordRequestPage(surfer: args.surfer);
          case Routes.loggedOutSurferChangePasswordRequest:
            return LoggedOutSurferChangePasswordRequestPage();
          case Routes.surferPrivacy:
            return const MobilePolicyPage(
                audience: 'surfer', policy: 'privacy');

          case Routes.help:
            return HelpPage();

          case Routes.empty:
          case Routes.home:
            return HomePage();

          case Routes.signIn:
            return SignInPage();

          case Routes.warnings:
            return WarningsPage();

          default:
            throw Exception('Invalid route: ${settings.name}');
        }
      });
}
