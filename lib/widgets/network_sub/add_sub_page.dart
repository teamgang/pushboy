import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sliver_glue/sliver_glue.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkSubAddPage extends StatefulWidget {
  @override
  NetworkSubAddPageState createState() => NetworkSubAddPageState();
}

class NetworkSubAddPageState extends State<NetworkSubAddPage> {
  String _failureMsg = '';
  bool _initialLoad = true;
  bool _requestInProgress = false;
  final _textController = TextEditingController();
  String apiUrl = 'https://pushboi.io/s/';
  final SubDetails _subDetails = SubDetails();

  @override
  initState() {
    super.initState();
    _textController.text = apiUrl;
    _subDetails.init(todd: King.of(context).todd);
  }

  String? get _validateLink {
    if (!_link.startsWith(apiUrl)) {
      return 'Link must start with $apiUrl';
    }
    return null;
  }

  String get _link {
    return _textController.text;
  }

  @override
  Widget build(BuildContext context) {
    final todd = King.of(context).todd;

    return MobileSliverScaffold(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      sliverGlue: SliverPadding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        sliver: SliverGlueFixedList(widgets: <Widget>[
          const SizedBox(height: 24),
          const Text22(
            'Subscribe to a network',
          ),
          const SizedBox(height: 12),

          TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            controller: _textController,
            enabled: !_requestInProgress,
            key: const Key(XKeys.addSubscriptionLinkField),
            keyboardType: TextInputType.multiline,
            maxLength: 200,
            maxLines: 12,
            minLines: 1,
            style: Theme.of(context).textTheme.headline6,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Subscription URL',
            ),
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9!.:/]')),
            ],
            onChanged: (newValue) {
              if (_initialLoad) {
                setState(() {
                  _initialLoad = false;
                });
              }
              _subDetails.link = newValue;
              _subDetails.getNetworkAndChannelDetails(context);
            },
            validator: (value) {
              return _validateLink;
            },
          ),
          //

          //Observer(
          //builder: (_) => Padding(
          //padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          //child: Row(
          //children: <Widget>[
          //Checkbox(
          //value: _subDetails.subscribeToAllChannels,
          //onChanged: (bool? newValue) {
          //_subDetails.subscribeToAllChannels =
          //!_subDetails.subscribeToAllChannels;
          //},
          //),
          //const Expanded(
          //child: Text18('Subscribe to all channels of this network.',
          //maxLines: 4),
          //),
          //],
          //),
          //),
          //),

          Observer(
            builder: (_) => todd.surferId.isEmpty
                ? const SizedBox.shrink()
                : const SizedBox(height: 12),
          ),
          Observer(
            builder: (_) => todd.surferId.isEmpty
                ? const SizedBox.shrink()
                : Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Checkbox(
                          value: _subDetails.subscribeAsSurfer,
                          onChanged: (bool? newValue) {
                            _subDetails.subscribeAsSurfer =
                                !_subDetails.subscribeAsSurfer;
                          },
                        ),
                        const Expanded(
                          child: Text18('Save subscription to your account',
                              maxLines: 3),
                        ),
                      ],
                    ),
                  ),
          ),

          const SizedBox(height: 16),
          _initialLoad
              ? const SizedBox.shrink()
              : Observer(
                  builder: (_) => Text16(
                    _subDetails.subFoundDescription,
                    maxLines: 8,
                  ),
                ),
          const SizedBox(height: 16),

          Observer(
            builder: (_) => Column(children: <Widget>[
              const Text16(
                'Subscribe to all channels in this network?',
                maxLines: 8,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: _subDetails.subscribeToAllChannels,
                      onChanged: (bool? newValue) =>
                          _subDetails.subscribeToAllChannels = true,
                    ),
                    const Expanded(child: Text('Yes')),
                  ],
                ),
              ),
              //

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: !_subDetails.subscribeToAllChannels,
                      onChanged: (bool? newValue) =>
                          _subDetails.subscribeToAllChannels = false,
                    ),
                    const Expanded(child: Text('No')),
                  ],
                ),
              ),
            ]),
          ),
          const SizedBox(height: 16),

          Observer(
            builder: (_) => Center(
              child: FunctionCard(
                text: 'Subscribe now',
                onTap: _subDetails.isReadyToSubscribe
                    ? () {
                        _onSubmit(context);
                      }
                    : null,
              ),
            ),
          ),
          const SizedBox(height: 8),
          _failureMsg == ''
              ? const SizedBox.shrink()
              : Text(_failureMsg, style: Bast.failure),
        ]),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      var king = King.of(context);
      ApiResponse ares = await _subDetails.executeSubscribe(king);

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        if (!mounted) return;
        FocusScope.of(context).unfocus();
        if (!mounted) return;
        king.dad.networkSubProxy.getAndLoadNetworkSubIdsForInstanceId();
        king.snacker.addSnack(Snacks.subscriptionAdded);
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg = ares.peekErrorMsg(defaultText: 'Failed to subscribe.');
        });
      }
    }
  }
}
