import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sliver_glue/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkSubOverviewPage extends StatefulWidget {
  const NetworkSubOverviewPage({required this.networkSub});
  final NetworkSub networkSub;

  @override
  NetworkSubOverviewPageState createState() => NetworkSubOverviewPageState();
}

class NetworkSubOverviewPageState extends State<NetworkSubOverviewPage> {
  @override
  initState() {
    super.initState();
    King.of(context)
        .dad
        .channelSubProxy
        .getAndLoadChannelSubIdsByNetworkSubId(networkSubId: _sub.networkSubId);
  }

  NetworkSub get _sub {
    return widget.networkSub;
  }

  @override
  Widget build(BuildContext context) {
    final channelSubProxy = King.of(context).dad.channelSubProxy;

    final channelSubIds =
        channelSubProxy.getChannelSubIdsRef(networkSubId: _sub.networkSubId);

    return MobileSliverScaffold(slivers: <Widget>[
      SliverToBoxAdapter(
        child: NetworkSubHeader(networkSub: widget.networkSub),
      ),
      SliverGlueObservableList(
        data: channelSubIds,
        reversed: true,
        builder: (context, _, index, isFirst, isLast) {
          return Observer(builder: (_) {
            final channelSubId = channelSubIds[index];

            return ChannelSubSlab(
              index: index,
              channelSub:
                  channelSubProxy.getChannelSubRef(channelId: channelSubId),
            );
          });
        },
      ),
    ]);
  }
}

class NetworkSubHeader extends StatefulWidget {
  const NetworkSubHeader({required this.networkSub});
  final NetworkSub networkSub;

  @override
  NetworkSubHeaderState createState() => NetworkSubHeaderState();
}

class NetworkSubHeaderState extends State<NetworkSubHeader> {
  @override
  Widget build(BuildContext context) {
    final network = widget.networkSub.network;

    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BasedFadeInImage(
            altText: network.abbrev,
            colorBackground: network.colorPrimary,
            colorText: network.colorSecondary,
            height: 200,
            width: 200,
            src: network.thumbSelectedUrl,
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: BaIconButton(
              iconData: Icons.settings,
              text: 'Settings',
              onTap: () {
                Navigator.of(context).pushNamed(Routes.networkSubSettings,
                    arguments: Args(networkSub: widget.networkSub));
              },
            ),
          ),
        ],
      ),
    );
  }
}
