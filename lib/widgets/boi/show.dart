import 'package:flutter/material.dart';
import 'package:sliver_glue/sliver_glue.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BoiShowPage extends StatefulWidget {
  const BoiShowPage({required this.boi});
  final Boi boi;

  @override
  _BoiShowPageState createState() => _BoiShowPageState();
}

class _BoiShowPageState extends State<BoiShowPage> {
  @override
  initState() {
    super.initState();
    final boi = widget.boi;
    if (!boi.isLoaded) {
      boi.loadFromApi(context, boi.boiId);
    }
  }

  @override
  Widget build(BuildContext context) {
    final boi = widget.boi;

    return MobileSliverScaffold(
      slivers: <Widget>[
        SliverGlueFixedList(widgets: <Widget>[
          Text(boi.title),
          TextButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamed(Routes.flag, arguments: Args(boi: boi));
            },
            child: const Text(
              'Report',
              style: TextStyle(color: Colors.grey, fontSize: 18),
            ),
          ),
        ]),
      ],
    );
  }
}
