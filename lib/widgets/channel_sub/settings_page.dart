import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:poboy/widgets/shared/select_sound.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelSubSettingsPage extends StatefulWidget {
  const ChannelSubSettingsPage({required this.channelSub});
  final ChannelSub channelSub;

  @override
  ChannelSubSettingsPageState createState() => ChannelSubSettingsPageState();
}

class ChannelSubSettingsPageState extends State<ChannelSubSettingsPage> {
  late final ChannelSub _copy;
  String _failureMsg = '';
  bool _hasAnyValueChanged = false;
  bool _requestInProgress = false;

  @override
  initState() {
    super.initState();
    _sub.syncValues();
    final king = King.of(context);
    _copy = king.dad.channelSubProxy
        .getChannelSubRef(channelId: king.conf.zerosUuid);
    _copy.loadFromAlt(_sub);
  }

  ChannelSub get _sub {
    return widget.channelSub;
  }

  bool _canSync(Todd todd) {
    return todd.isSignedIn && _sub.isLinkedToSurfer;
  }

  _toggleSilenced() {
    _copy.isSilenced = !_copy.isSilenced;
    _checkForValueChanges();
  }

  _changeSurferSound(SoundResource value) {
    _copy.surferSound = value;
    _checkForValueChanges();
  }

  _toggleUseSurferSound() {
    _copy.useSurferSound = !_copy.useSurferSound;
    _checkForValueChanges();
  }

  _toggleSynced() {
    _copy.syncEnabled = !_copy.syncEnabled;
    _checkForValueChanges();
  }

  _checkForValueChanges() {
    setState(() {
      _hasAnyValueChanged = !_sub.deepEquals(_copy);
    });
  }

  @override
  Widget build(BuildContext context) {
    final todd = King.of(context).todd;

    return MobileScaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: ElevatedButton(
                  onPressed: _hasAnyValueChanged
                      ? () {
                          _saveChanges(context);
                        }
                      : null,
                  child: const Text18('Save changes'),
                ),
              ),
              FailureMsg(_failureMsg),
              //

              _canSync(todd)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            value: _copy.syncEnabled,
                            onChanged: todd.isSignedIn
                                ? (bool? newValue) {
                                    _toggleSynced();
                                  }
                                : null,
                          ),
                          const Expanded(
                            child: Text18(
                                'Sync these settings with my other devices',
                                maxLines: 4),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),

              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: _copy.isSilenced,
                      onChanged: (bool? newValue) {
                        _toggleSilenced();
                      },
                    ),
                    const Text18('Silence this channel'),
                  ],
                ),
              ),

              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: _copy.useSurferSound,
                      onChanged: (bool? newValue) {
                        _toggleUseSurferSound()();
                      },
                    ),
                    const Text18('Use unique notification sound', maxLines: 3),
                  ],
                ),
              ),

              _copy.useSurferSound
                  ? InkWell(
                      onTap: () async {
                        final newSound = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SelectSoundPage()),
                        );
                        _changeSurferSound(newSound);
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(32, 24, 32, 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            const Text20('Sound: '),
                            Text20(
                              _copy.surferSound.displayName,
                              style: _copy.surferSound == SoundResource.none
                                  ? const TextStyle(
                                      color: Colors.grey,
                                      overflow: TextOverflow.ellipsis,
                                      fontStyle: FontStyle.italic,
                                    )
                                  : const TextStyle(
                                      color: Colors.black,
                                      overflow: TextOverflow.ellipsis,
                                      fontStyle: FontStyle.italic,
                                    ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const Center(
                      child: Text16('Using device notification sound',
                          style: TextStyle(
                            color: Colors.grey,
                            overflow: TextOverflow.ellipsis,
                            fontStyle: FontStyle.italic,
                          )),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  _saveChanges(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      _sub.loadFromAlt(_copy);
      _sub.saveLocal();
      var ares = await _sub.saveToRemote();

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        _sub.loadFromApi(); // refresh to make for UX
        if (!mounted) return;
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg = 'Failed to save changes';
        });
      }
    }
  }
}
