import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sliver_glue/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

//NOTE: really, this is the show bois page
class ChannelSubOverviewPage extends StatefulWidget {
  const ChannelSubOverviewPage({required this.channelSub});
  final ChannelSub channelSub;

  @override
  ChannelSubOverviewPageState createState() => ChannelSubOverviewPageState();
}

class ChannelSubOverviewPageState extends State<ChannelSubOverviewPage> {
  @override
  Widget build(BuildContext context) {
    final channel = widget.channelSub.channel;
    final bois = King.of(context).dad.bois.getBoisOfChannel(channel.channelId);

    return MobileSliverScaffold(slivers: <Widget>[
      SliverToBoxAdapter(
        child: ChannelSubHeader(channelSub: widget.channelSub),
      ),
      SliverGlueObservableList(
        data: bois,
        reversed: true,
        builder: (context, _, index, isFirst, isLast) {
          final boi = bois[index];
          return boi.isHidden
              ? const SizedBox.shrink()
              : BoiSlab(
                  index: index,
                  boi: boi,
                );
        },
      ),
    ]);
  }
}

class ChannelSubHeader extends StatefulWidget {
  const ChannelSubHeader({required this.channelSub});
  final ChannelSub channelSub;

  @override
  ChannelSubHeaderState createState() => ChannelSubHeaderState();
}

class ChannelSubHeaderState extends State<ChannelSubHeader> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  ChannelSub get _sub {
    return widget.channelSub;
  }

  @override
  Widget build(BuildContext context) {
    final channel = _sub.channel;

    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
      child: Observer(
        builder: (_) => Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              BasedFadeInImage(
                altText: channel.abbrev,
                colorBackground: channel.colorPrimary,
                colorText: channel.colorSecondary,
                height: 200,
                width: 200,
                src: channel.thumbSelectedUrl,
              ),
              Column(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: BaIconButton(
                    iconData: Icons.settings,
                    color: _sub.isSubscribed ? null : Colors.grey,
                    text: 'Settings',
                    onTap: _sub.isSubscribed
                        ? () {
                            Navigator.of(context).pushNamed(
                                Routes.channelSubSettings,
                                arguments: Args(channelSub: _sub));
                          }
                        : null,
                  ),
                ),
                //
              ]),
            ],
          ),
          const SizedBox(height: 12),
          Text18(channel.name,
              style: const TextStyle(overflow: TextOverflow.ellipsis)),
          const SizedBox(height: 12),

          Padding(
            padding: const EdgeInsets.all(10),
            child: Observer(
              builder: (_) => BaIconButton(
                iconData: Icons.settings,
                text: _sub.isSubscribed
                    ? 'Unsubscribe from this channel'
                    : 'Subscribe to this channel',
                onTap: () {
                  if (_sub.isSubscribed) {
                    _showConfirmUnsubscribeModal(context);
                  } else {
                    _onSubscribe(context);
                  }
                },
              ),
            ),
          ),
          //

          _sub.isSubscribed
              ? const Text('You receive notifications from this channel')
              : const Text(
                  'You do not receive notifications from this channel'),
          FailureMsg(_failureMsg),
        ]),
      ),
    );
  }

  void _showConfirmUnsubscribeModal(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: 200,
          width: 400,
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                      'Are you sure you want to unsubscribe from this channel?'),
                  const SizedBox(height: 12),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            child: const Text('Yes'),
                            onPressed: () {
                              _onUnsubscribe(context);
                              Navigator.of(context).pop(); // pop modal
                            }),
                        const SizedBox(width: 60),
                        ElevatedButton(
                          child: const Text('No'),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ]),
                ]),
          ),
        );
      },
    );
  }

  _onUnsubscribe(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      var king = King.of(context);
      ApiResponse ares = await king.lip.api(
        EndpointsV1.channelSubUnsubscribe,
        payload: {
          'channel_id': _sub.channelId,
          'network_sub_id': _sub.networkSubId,
        },
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        king.dad.channelSubProxy.getAndLoadChannelSubIdsByNetworkSubId(
            networkSubId: _sub.networkSubId);
      } else {
        setState(() {
          _failureMsg = 'Failed to unsubscribe.';
        });
      }
    }
  }

  _onSubscribe(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.channelSubSubscribe,
        payload: {
          'channel_id': _sub.channel.channelId,
          'network_sub_id': _sub.networkSubId,
        },
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        king.dad.channelSubProxy.getAndLoadChannelSubIdsByNetworkSubId(
            networkSubId: _sub.networkSubId);
      } else {
        setState(() {
          _failureMsg = 'Failed to subscribe.';
        });
      }
    }
  }
}
