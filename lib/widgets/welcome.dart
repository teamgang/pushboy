import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: <Widget>[
          const SizedBox(height: 24),
          const Center(
            child: Text20(
              'Welcome to PushBoi!',
            ),
          ),
          const SizedBox(height: 24),
          const Text(
              'A simple way to receive and manage push notifications from anywhere, in one place.',
              textAlign: TextAlign.center),
          const SizedBox(height: 20),
          //

          const Text('Use PushBoi anonymously or with an account.'),
          const SizedBox(height: 60),
          const Text('To get started: '),
          const SizedBox(height: 8),
          TextButton(
            child: const Text('Add a subscription'),
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.networkSubAdd);
            },
          ),
          const Text('or'),
          TextButton(
            child: const Text('Sign in or create an account'),
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.signIn);
            },
          ),
        ],
      ),
    );
  }
}
