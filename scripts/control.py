#!/usr/bin/env python

import json
import requests


headers = {
    'Content-Type': 'application/json',
}

data = {
    'network_access_token': 'WTJw8pNeAHY8zbmy',
    'channel_route': 'userGoesLive',
}

resp = requests.post(
    'http://127.0.0.1:8000/api/v1/dev/notification/send',
    headers=headers,
    data=json.dumps(data)
)

print(resp.status_code)
if resp.status_code != 200:
    raise RuntimeError('ERROR: request failed')
else:
    print(resp.json())
